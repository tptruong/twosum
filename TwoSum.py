#!python

#print 'abc'[::-1]
class Solution(object):
	def twoSum(self, nums, target):
		snums = sorted(nums)
		i = 0
		while i in range(len(snums)):
			if snums[i] > target and target > 0:
				a = target - snums[i]
				b = 0
				while b in range(len(snums)):
					if a < snums[b]:
						b = len(snums)
					elif a == snums[b]:
						d = 0
						while d in range(len(nums)):
							if nums[d] == snums[i]:
								e = 0
								while e in range(len(nums)):
									if nums[e] == snums[b]:
										if e == d:
											e += 1
										elif e > d:
											return [d, e]
										else:
											return [e, d]
									else:
										e += 1
							else:
								d += 1
					else:
						b += 1
					i += 1
			else:
				x = 0
				c = target - snums[i]
				while x in range(len(snums)):
					if snums[x] > c:
						x = len(snums)
					elif c == snums[x]:
						f = 0
						while f in range(len(nums)):
							if nums[f] == snums[i]:
								g = 0
								while g in range(len(nums)):
									if nums[g] == snums[x]:
										if g == f:
											g += 1
										elif g > f:
											return [f, g]
										else:
											return [g, f]
									else:
										g += 1
							else:
								f += 1
					else:
						x += 1
					i += 1