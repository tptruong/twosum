# Running the program should return the same list nums but in sorted order
# $ python binary_search.py 
# sorted array: [2, 7, 9, 15]
# 2
# -1 # means not found
class Solution(object):
	def sort(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		# Put your favorite sorting algorithm here
		return nums

	def binary_search(self, nums, target):
		"""
		:type nums: List[int]
		:rtype: index into nums of target
		"""
		return 0
        		 

# Testing
mySolution = Solution()
answer = mySolution.sort([15,9,7,2]) # For binary search to work, you must sort the array first
print answer
index = mySolution.binary_search(answer, 9)
print index
index = mySolution.binary_search(answer, 8)
print index
