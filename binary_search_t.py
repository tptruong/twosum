# Running the program should return the same list nums but in sorted order
# $ python binary_search.py 
# sorted array: [2, 7, 9, 15]
# 2
# -1 # means not found
class Solution(object):
	def sort(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		# Put your favorite sorting algorithm here
		nums.sort()
		return nums

	def binary_search(self, nums, target):
		"""
		:type nums: List[int]
		:rtype: index into nums of target
		"""
		a = len(nums)/2
		while a in range(len(nums)):
			if nums[a] == target:
				return a
			elif nums[a] > target:
				a = a / 2
			elif nums[a] > target and nums[a - 1] < target:
				return -1
			elif nums[a] < target and nums[a + 1] > target:
				return -1
			elif a == 0 and nums[a] != target:
				return -1
			elif a == len(nums) and nums[a] != target:
				return -1
			else:
				a = a + (a / 2)

	def binary_search2(self, nums, target):
		"""
		:type nums: List[int]
		:rtype: index into nums of target
		"""
		l = 0
		r = len(nums) - 1
		while l <= r:
			a = (r + l)/2
			if nums[a] == target:
				return a
			elif nums[a] > target:
				r = a - 1
			else:
				l = a + 1 
		return -1	 

# Testing
mySolution = Solution()
answer = mySolution.sort([15,9,7,2]) # For binary search to work, you must sort the array first
print answer
index = mySolution.binary_search2(answer, 9)
print index
index = mySolution.binary_search2(answer, 8)
print index

answer = mySolution.sort([2, 3, 4, 10, 40]) # For binary search to work, you must sort the array first
print answer
index = mySolution.binary_search2(answer, 10)
print index
index = mySolution.binary_search2(answer, 5)
print index

