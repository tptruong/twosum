# Your algorithm is good and clean. But it can be much faster. 
# 1) What is the run time of (how long it takes to find the answer using) your algorithm?
# 2) Can you do better by using binary search?
# 3) What is the run time using binary search?

def dupfinder(x):
	a = 0
	for a in range(len(x)):
		dup = 0
		if x[a] == x[a + 1]:
			dup = x.count(x[a])
			return dup
		else:
			a += 1

answer = dupfinder([1,2,3,3,3,3,3,3,4,7,9])
print answer