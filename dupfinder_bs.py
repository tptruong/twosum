class Solution(object):
	def dupfinder(self, low, high, target, x):
		if high >= low: 
			mid = (high + low) // 2
			ms = mid
			if x[mid] == target: 
				dupcount = 1
				for a in x:
					print ("element: ", a)
					if ms >= len(x) - 1 or mid <= 0:
						return dupcount
					elif x[ms] == x[ms+1]:
						dupcount += 1
						ms += 1
					elif x[mid] == x[mid-1]:
						dupcount += 1
						mid -= 1
					else:
						return dupcount
			elif x[mid] > target: 
				return self.dupfinder(low, mid - 1, target, x) 
			else: 
				return self.dupfinder(mid + 1, high, target, x) 
		else: 
			return -1
	
mySolution = Solution()
x = [1,2,3,3,3,3,3,3,4,7,9]
answer = mySolution.dupfinder(0, len(x) - 1, 3, x)
print answer
x = [2,5,5,5,6,6,8,9,9,9]
answer = mySolution.dupfinder(0, len(x) - 1, 5, x)
print answer
x = [2,5,5,5,6,6,8,9,9,9]
answer = mySolution.dupfinder(0, len(x) - 1, 6, x)
print answer
x = [2,5,5,5,6,6,8,9,9,9]
answer = mySolution.dupfinder(0, len(x) - 1, 9, x)
print answer
x = [1, 2, 2, 2, 2, 3, 4, 5, 5, 5, 5]
answer = mySolution.dupfinder(0, len(x) - 1, 3, x)
print answer