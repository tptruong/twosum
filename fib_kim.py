# Running the program should return this answer: 
# $ python fib.py 
# 2
# 3
# 5
# 8
# 13
# 21
# 34
#
# This is the fibonacci sequence:
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34
class Solution(object):
    def fib(self, n):
        """
        :type n: int
        :rtype: int
        """
        answer = 0
        if n == 0:
        	return 0
        elif n == 1:
        	return 1
        else:
        	return self.fib(n-1) + self.fib(n-2)
        
# Testing
mySolution = Solution()
answer = mySolution.fib(3)
print answer
answer = mySolution.fib(4)
print answer
answer = mySolution.fib(5)
print answer
answer = mySolution.fib(6)
print answer
answer = mySolution.fib(7)
print answer
answer = mySolution.fib(8)
print answer
answer = mySolution.fib(9)
print answer