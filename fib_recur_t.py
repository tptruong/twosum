# Running the program should return this answer: 
# $ python fib.py 
# 2
# 3
# 5
# 8
# 13
# 21
# 34
#
# This is the fibonacci sequence:
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34

class Solution(object):
	def fib(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		if n == 0:
			answer = 0
		elif n == 1:
			answer = 1
		else:
			answer = self.fib(n-2) + self.fib(n-1)
		return answer
		            	       
# Testing
mySolution = Solution()
answer = mySolution.fib(3)
print answer
answer = mySolution.fib(4)
print answer
answer = mySolution.fib(5)
print answer
answer = mySolution.fib(6)
print answer
answer = mySolution.fib(7)
print answer
answer = mySolution.fib(8)
print answer
answer = mySolution.fib(9)
print answer