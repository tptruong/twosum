# Running the program should return the same list nums but in sorted order
# $ python insertion.py 
# [2, 7, 9, 15]
#
class Solution(object):
	def swap(self, nums, i, j):
		# This is a helper method to make the code easier to read since you will need to call it a lot
		return

	def insertion(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		# HINT: to call swap, do:
		# self.swap(nums, index1, index2)
		return nums


# Testing
mySolution = Solution()
answer = mySolution.insertion([15,9,7,2])
print answer