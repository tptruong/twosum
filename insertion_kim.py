# Running the program should return the same list nums but in sorted order
# $ python insertion.py 
# [2, 7, 9, 15]
#
# 1st iteration (i = 0, j = 1): [9, 15, 7, 2]
# 2nd (i = 1, j = 2): [9, 7, 15, 2]
# 3rd (i = 1, j = 1): [7, 9, 15, 2]
# 4th (i = 2, j = 3): [7, 9, 2, 15]
# 5th (i = 2, j = 2): [7, 2, 9, 15]
# 6th (i = 2, j = 1): [2, 7, 9, 15]
# 7th (i = 3, j = 4): j == len(nums). So wont go into while loop
class Solution(object):
	def swap(self, nums, i, j):
		tmp = nums[i]
		nums[i] = nums[j]
		nums[j] = tmp
		return

	def insertion(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		i = 0
		j = i + 1 # invariant/rule: everything to left of j is sorted
		for x in nums:
			print nums
			while j > 0 and j < len(nums):
				if nums[j] < nums[j-1]: # keep moving back and swap until we've reached the 0th element or it's no longer smaller than the previous element
					self.swap(nums, j, j-1) # to call a method belonging to the class, always use self.<method_name>
				j = j - 1
			i = i + 1
			j = i + 1
		return nums


# Testing
mySolution = Solution()
answer = mySolution.insertion([15,9,7,2])
print answer