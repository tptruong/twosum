# Running the program should return the same list nums but in sorted order
# $ python insertion.py 
# [2, 7, 9, 15]
#
class Solution(object):
	def swap(self, nums, i, j):
		# This is a helper method to make the code easier to read since you will need to call it a lot
		a = nums[i]
		nums[i] = nums[j]
		nums[j] = a
		return nums

	def insertion(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		# HINT: to call swap, do:
		# self.swap(nums, index1, index2)
		a = 0
		x = 0
		big = 0
		while a in range(len(nums)):
			if a + 1 == len(nums):
				return nums
			else:
				lowest = 999999
				b = a + 1
				print b
				while b in range(len(nums)):
					if nums[a] > nums[b]:
						print "break2"
						print lowest
						print nums[b]
						if lowest > nums[b]:
							print "break3"
							print lowest
							lowest = nums[b]
							x = b
							b += 1
						else:
							b += 1
					else:
						b += 1
				self.swap(nums, a, x)
				a += 1
		return nums


# Testing
mySolution = Solution()
answer = mySolution.insertion([15,9,7,2])
print answer