class Solution(object):
	def minAddToMakeValid(self, S):
		l = list(S)
		a = 0
		aset = 0
		no_set = 0
		while a in range(len(l)):
			c = a + 1
			e = ""
			if a >= len(l) - 1 and l[a] == "(":
				no_set += 1
				a += 1
			elif l[a] == "(":
				while c in range(len(l)):
					if c == a + 1 and l[c] == ")":
						aset += 1
						c += len(l)
						a += 1
					elif len(l) > c > a + 1 and l[c] == ")":
						e = l[a]
						l[a] = l[c]
						l[c] = e
						a += 1
						c += len(l)
					elif c >= len(l) - 1 and l[c] == "(":
						no_set += 1
						c += 1
					else:
						c += 1
				a += 1
			else:
				no_set += 1
				a += 1
		return no_set


mySolution = Solution()
answer = mySolution.minAddToMakeValid("())")
print answer
mySolution = Solution()
answer = mySolution.minAddToMakeValid("(((")
# print "The answer is"
print answer
mySolution = Solution()
answer = mySolution.minAddToMakeValid("()")
print answer
mySolution = Solution()
answer = mySolution.minAddToMakeValid("()))((")
print answer
answer = mySolution.minAddToMakeValid("(()))))(")
print answer