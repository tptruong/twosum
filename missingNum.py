#Find the missing number in the List
class Solution(object):
	def missingNum(self, minnum, maxnum, x):
		x.sort()
		answer = 0
		if maxnum >= minnum:
			mid = ((maxnum + minnum)/2)
			if mid not in x:
				answer = mid
				return answer
			elif mid > maxnum/2:
				return self.missingNum(mid, maxnum, x)
			else:
				return self.missingNum(minnum, mid, x)
		else:
			return -1

		
		
mySolution = Solution()
answer = mySolution.missingNum(1, 9, [1,3,2,5,6,4,7,9])
print answer
answer = mySolution.missingNum(1,10,[1,3,2,5,6,8,7,9,10])
print answer