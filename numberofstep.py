class Solution(object):
	def numberOfSteps (self, num):
		steps = 0
		while num > 0:
			if num % 2 == 0:
				num = num  / 2
				steps += 1
			else:
				num = num - 1
				steps += 1
		return steps

mySolution = Solution()
answer = mySolution.numberOfSteps(14)
print answer
answer = mySolution.numberOfSteps(15)
print answer
answer = mySolution.numberOfSteps(8)
print answer
answer = mySolution.numberOfSteps(40)
print answer
answer = mySolution.numberOfSteps(140)
print answer
