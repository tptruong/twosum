##### FEEDBACK
#
# 1) This test case is incorrect. The output should be 2, not 3:
# 	answer = mySolution.pairSum([3, -2, 1])
# 	print answer
# 2) Look at your code and see if you can simplify what happens in your if and else statement
# 3) Do you know the RUN TIME of your algorithm? 
# 	 	a) Is it O(n) (meaning it searches through entire array)
#		b) Is it O(log n) (meaning it's roughly half the number of elements in the arrary)
#		c) Is it O(n^2) (meaning it's QUADRATIC. So if n=2, then it's 4. If n=8, then it's 64!)
# Other than those 3 comments, it looks great! Great job! Answer these these comments and send back to me.  

# Number of pairs in an array with the sum greater than 0
#
# Given an array arr[] of size N, the task is to find the number of distinct pairs in the array whose sum is > 0.
#
#Examples:
#
#Input: arr[] = { 3, -2, 1 }
#Output: 2
#Explanation:
#There are two pairs of elements in the array whose sum is positive. They are:
#{3, -2} = 1
#{3, 1} = 4
#
#Input: arr[] = { -1, -1, -1, 0 }
#Output: 0
#Explanation:
#There are no pairs of elements in the array whose sum is positive.
#
# Solution: https://www.geeksforgeeks.org/number-of-pairs-in-an-array-with-the-sum-greater-than-0/?ref=leftbar-rightbar

class Solution(object):
	def pairSum(self, x):
		a = 0
		pair = 0
		while a in range(len(x)):
			b = a + 1
			while b in range(len(x)):
				if x[a] + x[b] > 0:
					pair += 1
				b += 1
			a += 1
		return pair

mySolution = Solution()
answer = mySolution.pairSum([3, -2, 1])
print answer
answer = mySolution.pairSum([-1, -1, -1, 0])
print answer
answer = mySolution.pairSum([3, -2, 1, -1, -1, -1, 0])
print answer
timer = timeit.timeit(pairSum, number=1)
print timer