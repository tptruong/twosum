# Can you write a recursive function to find factorial number?
# Factorial of n means n * (n-1) * (n-2) * ... * 1 = 
# So factorial of 3 means 3 * 2 *1 = 6.

def fact( n ):
	# Algorithm here
	return n
answer = fact( 3 )
print answer

# Can you find fibonacci's number recursively? Fib ( n ) = fib ( n - 1 ) + fib ( n - 2 )
# So fib( 3 ) = fib ( 2 ) + fib( 1 ) = 1 + 1 = 2
# Your base cases (smallest case that exits the recursion) are: fib( 0 ) = 0, fib( 1 ) = 1
def fib( n ):
	# Algorithm here
	return n
answer = fib( 3 )
print answer
