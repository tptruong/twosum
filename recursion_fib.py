def fact( n ):
	if n == 0 or n == 1:
		n = 1
		return n
	else:
		n = n * fact ( n - 1 )
	return n
	
answer = fact( 3 )
print answer
answer = fact( 4 )
print answer
answer = fact( 5 )
print answer
answer = fact( 6 )
print answer

# Can you find fibonacci's number recursively? Fib ( n ) = fib ( n - 1 ) + fib ( n - 2 )
# So fib( 3 ) = fib ( 2 ) + fib( 1 ) = 1 + 1 = 2
# Your base cases (smallest case that exits the recursion) are: fib( 0 ) = 0, fib( 1 ) = 1
def fib( n ):
	if n == 0:
		n = 0
		return n
	elif n == 1:
		n = 1
		return n
	else:
		n = fib (n - 1) + fib ( n - 2)
	return n
	
answer = fib( 3 )
print answer
answer = fib( 4 )
print answer
answer = fib( 5 )
print answer
answer = fib( 6 )
print answer
