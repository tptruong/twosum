class Solution(object):
	def reverse(self, x):
		"""
		:type x: int
		:rtype: int
		"""
		a = abs(x)
		b = 0
		answer = 0
		nums = []
		while a > 0:
			nums.append(a % 10)
# 			print nums
			a = a / 10
		while b in range(len(nums)):
			answer = 10 * answer + nums[b]
			b += 1
		if answer > 2 ** 31:
			print "0"
			return 0
		elif x < 0:
			print "-" + str(answer)
			return -answer
		else:
			print answer
			return answer

solution = Solution()
answer = solution.reverse(123)
answer = solution.reverse(-123)
answer = solution.reverse(120)
answer = solution.reverse(1534236469)