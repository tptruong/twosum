#!python

# Running the program should return this answer: 
# $ python reverse.py
# ['c', 'b', 'a']
class Solution(object):
    def reverse(self, characters):
        """
        :type characters: List[int]
        :rtype: List[int]
        """
        answer = []
        x = len(characters)
        for x in characters:
        	answer.insert(0, x)
        return answer
        
# Testing
mySolution = Solution()
answer = mySolution.reverse(['a','b','c'])
print answer