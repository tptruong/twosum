# Running the program should return the same list nums but in sorted order
# $ python selection.py 
# [2, 7, 9, 15]
#
class Solution(object):
    def selection(self, nums):
        """
        :type nums: List[int]
        :rtype: nums
        """
        return nums
        		 

# Testing
mySolution = Solution()
answer = mySolution.selection([15,9,7,2])
print answer