# Sort this array using the algorithm called selection sort:
# 1) Find the minimum element in array
# 2) Swap that element with the element at the beginning of the array 'p'.
# 3) Increment pointer 'p' to the next element of the array.
#    So anything before 'p' is sorted.
# 4) Repeat 1, 2, 3
#
# Example: arr[] = 64 25 12 22 11
# // Find the minimum element in arr[0...4]
#// and place it at beginning
#11 25 12 22 64
#
#// Find the minimum element in arr[1...4]
#// and place it at beginning of arr[1...4]
#11 12 25 22 64
#
#// Find the minimum element in arr[2...4]
#// and place it at beginning of arr[2...4]
#11 12 22 25 64
#
#// Find the minimum element in arr[3...4]
#// and place it at beginning of arr[3...4]
#11 12 22 25 64 
# 
# Solution here: https://www.geeksforgeeks.org/selection-sort/
