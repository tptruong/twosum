# Running the program should return the same list nums but in sorted order
# $ python selection.py 
# [2, 7, 9, 15]
#
class Solution(object):
    def selection(self, nums):
        """
        :type nums: List[int]
        :rtype: nums
        """
        minIndex = 0
        j = 0
        for y in nums:
        	print nums
        	i = j + 1
        	for x in nums[i:]:
        		if x < nums[minIndex]:
        			tmp = nums[minIndex]
        			nums[minIndex] = x
        			nums[i] = tmp
        		i = i + 1
        	j = j + 1
        	minIndex = j
        return nums
        		 

# Testing
mySolution = Solution()
answer = mySolution.selection([15,9,7,2])
print answer
