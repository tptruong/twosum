#!python

# Running the program should return the same list nums but in sorted order
# $ python selection.py
# [2, 7, 9, 15]
#
class Solution(object):
	def selection(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		a = 0
		x = 0
		big = 0
		while a in range(len(nums)):
			if a + 1 == len(nums): # Shouldn't need to special case for the condition that the array has only 1 element. 
				return nums
			else:
				lowest = 999999 # Best to not assume this because what if all the elements are > 999999? Better to just assume the first element is the smallest
				b = a + 1
				print b
				while b in range(len(nums)):
					if nums[a] > nums[b]: # No need to check this condition. Can just check if any nums[b] is smaller than the lowest number so far for b > a
						print "break2"
						print lowest
						print nums[b]
						if lowest > nums[b]: 
							print "break3"
							print lowest
							lowest = nums[b]
							x = b
							b += 1 # Increment b if true
						else:
							b += 1 # Increment b if false. This means you increment b no matter what
					else:
						b += 1
        		big = nums[a] # These 3 liners are a swap routine, which shouldn't need to do unless you found a "lowest" number lower than your initial assumption.  
    			nums[a] = lowest
        		nums[x] = big
        		a += 1
        		print nums 
		return nums # At the end, you need to return nums
        		
	def selection2(self, nums):
		"""
		:type nums: List[int]
		:rtype: nums
		"""
		a = 0
		x = 0
		big = 0
		while a in range(len(nums)):
			lowest = nums[a] # assume the first one is the smallest. 
			b = a + 1
			#print nums
			while b in range(len(nums)): # find the lowest number starting from a + 1
				if lowest > nums[b]:
					lowest = nums[b]
					x = b
				b += 1 # You increment b no matter what.
			if lowest != nums[a]: # if lowest is no longer nums[a], then we have found a lower number. Swap it. Otherwise, move onto the next index a + 1
				big = nums[a]
				nums[a] = lowest
				nums[x] = big
			a += 1
		return nums

# Testing
mySolution = Solution()
answer = mySolution.selection([15,9,7,2,10,3,6])
print answer
answer = mySolution.selection2([15,9,7,2,10,3,6])
print answer
answer = mySolution.selection2([15])
print answer