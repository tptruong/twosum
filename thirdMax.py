class Solution(object):
	def thirdMax(self, nums):
		nums.sort(reverse=True)
 		a = 0
 		i = 1
 		while i in range(len(nums)):
 			if nums[a] == nums [i]:
 				nums.pop(i)
 			elif nums[i] == nums[i - 1]:
 				nums.pop(i)
 			else:
 				i += 1
 		while a in range(len(nums)):
			if len(nums) <= 2:
				return nums[a]
			else:
				return nums[a + 2]

				

mySolution = Solution()
answer = mySolution.thirdMax([14,1,3,6,8,2,1])
print answer
answer = mySolution.thirdMax([14,14,1,3,6,8,2,8,8,6,1])
print answer
answer = mySolution.thirdMax([1,2])
print answer
answer = mySolution.thirdMax([3,1231])
print answer
answer = mySolution.thirdMax([2,2,2,2,2,1,1])
print answer
answer = mySolution.thirdMax([1,1,1])
print answer

