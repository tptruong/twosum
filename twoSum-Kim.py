#
#
# Runtime: 4380 ms, faster than 26.12% of Python online submissions for Two Sum.
# Memory Usage: 12.6 MB, less than 76.37% of Python online submissions for Two Sum.
#
# For example, given an list [2, 7, 11, 15]
# This algorithm would check for x = 2:
#     2 + 7 == target
#     2 + 11 == target
#     2 + 15 == target
# Then exit the second for loop and try the next x = 7:
#     7 + 11 == target
#     7 + 15 == target
# Then exit the second for loop and try the next x = 11:
#     11 + 15 == target
# Then exit the second for loop and try x = 15:
# Then exit the second for loop, and exit the first for loop
#
# To run the program, in Terminal do
# $ python twoSum-Kim.py 
# [0, 1]
class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        ans = []
        i = 1
        j = 0
        for x in nums: # iterate through all elements of the list
            i = j + 1 # for each element x, check if the sum of x and each element after x (called y)== target  
            for y in nums[i:]: # start checking on the element AFTER x 
                if( x + y == target):
                    ans.append(j) # add the index of x to the list of answer
                    ans.append(i) # add the index of y to the list of answer
                    return ans
                i = i + 1
            j = j + 1

# Testing
mySolution = Solution()
answer = mySolution.twoSum([2,7,11,15], 9)
print answer