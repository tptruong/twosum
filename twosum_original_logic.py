#!python

class Solution(object):
 	def twoSum(self, nums, target):
 		i = 0
 		while i in range(len(nums)):
 			x = i + 1
		   	while x in range(len(nums)):
		   		if nums[i] + nums[x] == target:
					return [i, x]
	    		else:
					x += 1
	    		i += 1